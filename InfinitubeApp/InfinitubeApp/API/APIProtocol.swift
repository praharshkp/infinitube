//
//  APIProtocol.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import UIKit

enum Result {
  case success(VideoList), failure(VideoListError)
}

enum VideoListError: Error {
  case notReachable,
  badURL,
  unableToFetch,
  requestFailed,
  noDataRecieved,
  parsingIssue,
  underlyingError(Error)
}

enum ImageDownloadResult {
  case success(UIImage), failure(ImageDownloadError)
}

enum ImageDownloadError: Error {
  case notReachable, emptyUrl, badURL, requestFailed, noDataRecieved, underlyingError(Error)
}

protocol APIProtocol {
  func getVideoList(parser: VideoListParser, completion: @escaping (Result) -> Void)
  func downloadImageAt(path: String?, completion: @escaping (ImageDownloadResult) -> Void)
}

protocol VideoListParser {
  func videoListFrom(data: Data) throws -> VideoList
}
