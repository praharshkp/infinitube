//
//  APIManager.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import Foundation
import UIKit

struct APIManager: APIProtocol {
  
  private let endPoint: String
  
  init(path: String) {
    endPoint = path
  }
  
  func getVideoList(parser: VideoListParser, completion: @escaping (Result) -> Void) {
    guard Reachability.isConnectedToNetwork() else {
      completion(.failure(.notReachable))
      return
    }
    guard let endPoint = URL(string: endPoint) else {
      completion(.failure(.badURL))
      return
    }
    let session = URLSession(configuration: URLSessionConfiguration.default,
                             delegate: nil,
                             delegateQueue: nil)
    
    var request = URLRequest(url: endPoint)
    request.httpMethod = Constants.Strings.HttpMethods.get
    
    let task = session.dataTask(with: request) {data, response, error in
      var result: Result
      
      defer {
        completion(result)
      }
      
      if let error = error {
        result = .failure(.underlyingError(error))
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
        result = .failure(.requestFailed)
        return
      }
      
      guard let data = data else {
        result = .failure(.noDataRecieved)
        return
      }
      do {
        let videosList = try parser.videoListFrom(data: data)
        result = .success(videosList)
      } catch let error {
        print(error)
        result = .failure(.parsingIssue)
      }
      
    }
    task.resume()
  }
  
  func downloadImageAt(path: String?, completion: @escaping (ImageDownloadResult) -> Void) {
    guard Reachability.isConnectedToNetwork() else {
      completion(.failure(.notReachable))
      return
    }
    guard let url = path else {
      completion(.failure(.emptyUrl))
      return
    }
    guard let endPoint = URL(string: url) else {
      completion(.failure(.badURL))
      return
    }
    let session = URLSession(configuration: .default)
    let downloadPicTask = session.dataTask(with: endPoint) { (data, response, error) in
      var result: ImageDownloadResult
      defer {
        completion(result)
      }
      if let error = error {
        result = .failure(.underlyingError(error))
        return
      }
      guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
        result = .failure(.requestFailed)
        return
      }
      guard let imageData = data else {
        result = .failure(.noDataRecieved)
        return
      }
      if let image = UIImage(data: imageData) {
        result = .success(image)
        return
      } else {
        result = .failure(.noDataRecieved)
        return
      }
    }
    downloadPicTask.resume()
  }
}
