//
//  VideoInfoParser.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import Foundation
import SwiftyJSON

class VideoInfoParser: VideoListParser {
  // MARK: Parser
  enum ParserError: Error { case parsingFailed }
  func videoListFrom(data: Data) throws -> VideoList {
    if let videoInfo = String(data: data, encoding: .ascii) {
      if let dlData = videoInfo.data(using: String.Encoding.utf8) {
        let json = try JSON(data: dlData)
        let title = json[Constants.JSONKeys.VideoInfo.title].stringValue
        let mappedImageInfo = json[Constants.JSONKeys.VideoInfo.rowArray].arrayValue.map(
        { (json) -> VideoInfoModel in
          return VideoInfoModel(json: json)
        })
        let filteredImageInfo = mappedImageInfo.filter({ (imageInfo) -> Bool in
          return imageInfo.valid
        })
        return VideoList(title: title, infos: filteredImageInfo)
      }
    }
    return VideoList(title: Constants.Strings.emptyString, infos: [])
  }
}
