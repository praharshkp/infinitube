//
//  VideoInfoViewModel.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import Foundation
import SwiftyJSON

class VideoListViewModel: VideoInfoProtocol {
  
  var header = Constants.Strings.loadingPleaseWait
  private var videoInfoStore: [VideoInfoModel] = []
  private var api: APIProtocol
  var delegate: ServiceUpdatable?
  init(api: APIProtocol) {
    self.api = api
  }
  func refresh() {
    refreshModelStarted()
    self.videoInfoStore.removeAll()
    api.getVideoList(parser: VideoInfoParser()) { (result) in
      switch result {
      case .success(let fact):
        self.header = fact.title
        self.videoInfoStore.append(contentsOf: fact.infos)
        self.refreshModelCompleted()
      case .failure(let error):
        switch error {
        case .unableToFetch, .requestFailed, .noDataRecieved, .parsingIssue:
          self.delegate?.showMessage(message: Constants.Strings.Messages.errorInReachingServer)
        case .underlyingError:
          self.delegate?.showMessage(message: Constants.Strings.Messages.errorInReachingServer)
        case .notReachable:
          self.delegate?.showMessage(message: Constants.Strings.Messages.errorInConnection)
        case .badURL:
          self.delegate?.showMessage(message: Constants.Strings.Messages.errorInParsingUrl)
        }
      }
    }
  }
  private func refreshModelStarted() {
    DispatchQueue.main.async {
      self.delegate?.dataRefreshStarted()
    }
  }
  private func refreshModelCompleted() {
    DispatchQueue.main.async {
      self.delegate?.dataRefreshCompleted()
    }
  }
  /*This func downloads an image at an index on demand. Once the download is completed, it will
   be loaded to the tableview, only if that particular row is currently visible */
  private func startDownloadForIndex(index: Int) {
    let imageInfo = self.videoInfoStore[index]
    api.downloadImageAt(path: imageInfo.imageUrl) { (result) in
      switch result {
      case .success(let image):
        DispatchQueue.main.async {
          imageInfo.loadImage(image: image)
          self.delegate?.updatedAtIndex(index: index)
        }
      case .failure(let error):
        imageInfo.imageDownloadStatus = .failed
        self.notifyError(error: error, index: index)
      }
    }
  }
  private func notifyError(error: ImageDownloadError, index: Int) {
    var message: String
    switch error {
    case .requestFailed, .noDataRecieved:
      message = Constants.Strings.Messages.errorInReachingServer
    case .underlyingError:
      message = Constants.Strings.Messages.errorInReachingServer
    case .notReachable:
      message = Constants.Strings.Messages.errorInConnection
    case .badURL:
      message = Constants.Strings.Messages.errorInParsingUrl
    case .emptyUrl:
      message = Constants.Strings.Messages.errorInParsingUrl
    }
    DispatchQueue.main.async {
      self.delegate?.updatedAtIndex(index: index)
      self.delegate?.showMessage(message: message)
    }
  }
  func rowCount() -> Int {
    return self.videoInfoStore.count
  }
  func videoInfoAtIndex(index: Int) -> VideoInfoModel? {
    guard index >= 0 && index < self.videoInfoStore.count else {
      return nil
    }
    let imageInfo = self.videoInfoStore[index]
    if imageInfo.imageDownloadStatus == .yetToStart {
      startDownloadForIndex(index: index)
    }
    return imageInfo
  }
}
