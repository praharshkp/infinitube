//
//  VideoInfoProtocol.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import Foundation

protocol VideoInfoProtocol {
  // This is the abstraction for a Facts Model.
  // Header is the top title in the UI
  var header: String {get}
  // Delegate should be updatable from withint the model
  var delegate: ServiceUpdatable? {get set}
  // Refresh will download a fresh content from the server
  func refresh()
  func rowCount() -> Int
  func videoInfoAtIndex(index: Int) -> VideoInfoModel?
}
