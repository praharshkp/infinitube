//
//  VideoInfoModel.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum ImageDownloadStatus {
  case yetToStart
  case started
  case downloaded
  case failed
  case notAvailable
}

struct VideoList {
  let title: String
  let infos: [VideoInfoModel]
}

class VideoInfoModel {
  let title: String
  let subTitle: String
  let imageUrl: String
  var imageDownloadStatus: ImageDownloadStatus = .yetToStart
  var image: UIImage?
  var valid = false
  
  init(json: JSON) {
    self.title = json[Constants.JSONKeys.VideoInfo.Rows.title].string ??
      Constants.Strings.defaultTitle
    self.subTitle = json[Constants.JSONKeys.VideoInfo.Rows.description].string ?? Constants.Strings.defaultDescription
    if let imageUrl = json[Constants.JSONKeys.VideoInfo.Rows.imageHref].string {
      self.imageUrl = imageUrl
    } else {
      self.imageUrl = Constants.Strings.emptyString
      self.imageDownloadStatus = .notAvailable
    }
    self.valid = !(json[Constants.JSONKeys.VideoInfo.Rows.title].string == nil &&
      json[Constants.JSONKeys.VideoInfo.Rows.description].string == nil &&
      json[Constants.JSONKeys.VideoInfo.Rows.imageHref].string == nil)
  }
  func loadImage(image: UIImage) {
    self.image = image
    self.imageDownloadStatus = .downloaded
  }
}
