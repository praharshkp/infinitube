//
//  HomeCoordinator.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import UIKit

class HomeCoordinator: UIViewController {
  
  override func viewDidAppear(_ animated: Bool) {
    self.start()
  }
  
  func start() {
    let api = APIManager(path: Constants.Strings.endPoint)
    let videosModel = VideoListViewModel(api: api)
    let homeViewController = HomeViewController(model: videosModel)
    videosModel.delegate = homeViewController
    self.navigationController?.pushViewController(homeViewController, animated: false)
  }
  
}
