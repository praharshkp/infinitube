//
//  ServiceUpdatable.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

protocol ServiceUpdatable {
  func dataRefreshStarted()
  func dataRefreshCompleted()
  func showMessage(message: String)
  func updatedAtIndex(index: Int)
}
