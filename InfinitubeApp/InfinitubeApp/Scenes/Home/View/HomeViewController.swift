//
//  HomeViewController
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import UIKit
import TTGSnackbar

class HomeViewController: UIViewController, ServiceUpdatable {
  
  private var model: VideoInfoProtocol
  private var tableView = UITableView()
  
  init(model: VideoListViewModel) {
    self.model = model
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    self.model = VideoListViewModel(api: APIManager(path: Constants.Strings.endPoint))
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.blue
    self.navigationItem.hidesBackButton = true
    
    //Tableview configuration, setup delegate and data source
    self.tableView.register(VideoInfoCell.self, forCellReuseIdentifier: Constants.CellNames.imageInfoCell)
    self.tableView.dataSource = self
    self.view.addSubview(self.tableView)
    updateViewConstraints()
    
    //Pull to refresh
    let refreshControl = UIRefreshControl()
    refreshControl.attributedTitle = NSAttributedString(string: Constants.Strings.loadingPleaseWait)
    refreshControl.addTarget(self, action: #selector(pullRefresh(sender:)), for: UIControl.Event.valueChanged)
    tableView.refreshControl = refreshControl
    self.model.delegate = self
    self.model.refresh()
  }
  
  override func loadView() {
    super.loadView()
    
    let tempTableView = UITableView(frame: .zero)
    tempTableView.translatesAutoresizingMaskIntoConstraints = false
    self.view.addSubview(tempTableView)
    NSLayoutConstraint.activate([
      tempTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
      tempTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
      tempTableView.topAnchor.constraint(equalTo: self.view.topAnchor),
      tempTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
      ])
    self.tableView = tempTableView
  }
  
  override func updateViewConstraints() {
    super.updateViewConstraints()
  }
  
  func dataRefreshStarted() {
    self.navigationItem.title = Constants.Strings.loadingPleaseWait
  }
  
  func dataRefreshCompleted() {
    self.tableView.refreshControl?.endRefreshing()
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationItem.title = self.model.header
    self.tableView.reloadData()
  }
  
  @objc func pullRefresh(sender: AnyObject) {
    self.model.refresh()
  }
  
  func updatedAtIndex(index: Int) {
    if let visibleRows = self.tableView.indexPathsForVisibleRows {
      for indexPath in visibleRows where indexPath.row == index {
        self.tableView.reloadData()
      }
    }
  }
  
  func showMessage(message: String) {
    let snackbar = TTGSnackbar(message: message, duration: .short)
    snackbar.show()
  }
}

extension HomeViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.model.rowCount()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cellViewModel = self.model.videoInfoAtIndex(index: indexPath.row) {
      if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellNames.imageInfoCell,
                                                  for: indexPath) as? VideoInfoCell {
        cell.loadImageInfo(videoInfo: cellViewModel)
        return cell
      }
    }
    return UITableViewCell()
  }
}
