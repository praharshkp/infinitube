//
//  VideoInfoCell.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import UIKit

class VideoInfoCell: UITableViewCell {
  
  @IBOutlet weak var picture: UIImageView!
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var details: UITextView!
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func loadImageInfo(videoInfo: VideoInfoModel) {
    self.label.text = videoInfo.title
    self.details.text = videoInfo.subTitle
    if let pic = videoInfo.image {
      self.picture.image = pic
    } else {
      switch videoInfo.imageDownloadStatus {
      case .failed, .downloaded:
        self.picture.image = Constants.Images.error
      case .started, .yetToStart:
        self.picture.image = Constants.Images.downloading
      case .notAvailable:
        self.picture.image = nil
      }
    }
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.clearsContextBeforeDrawing = true
    self.contentView.clearsContextBeforeDrawing = true
    self.label.text = nil
    self.details.text = nil
    self.picture.image = nil
  }
  
  func configureUI() {
    
//    label.numberOfLines = 0
//
//    details.font = UIFont(name: Constants.Fonts.AvenirBook, size: Constants.FontSizes.small)
//    details.textColor = UIColor.lightGray
    
  }
}
