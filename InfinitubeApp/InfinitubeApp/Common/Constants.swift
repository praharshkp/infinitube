//
//  Constants.swift
//  InfinitubeApp
//
//  Created by Praharsh Kalliyatt Panoli on 24/09/19.
//  Copyright © 2019 Praharsh Kalliyatt Panoli. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
  struct Images {
    /// Image `downloading`.
    static let downloading = UIImage(named: "downloading")
    /// Image `error`.
    static let error = UIImage(named: "error")
  }
  struct Strings {
    /// empty string
    static let emptyString = ""
    static let loadingPleaseWait = "Loading...Please Wait..."
    static let urlsBrokenError = "The URL is not reachable"
    static let someImagesUnavailable = "Some images are not available"
    static let someImagesCorrupted = "Some images are corrupted"
    static let someImageUrlsNonreachable = "Some image URLs are unreachable"
    static let defaultTitle = "No Title for this one"
    static let defaultDescription = "Nothing much about this"
    static let endPoint = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    struct Messages {
      static let errorInParsingUrl = "There was an error in the URL"
      static let errorInReachingServer = "There was an error in reaching server"
      static let errorInConnection = "You are not connected to Internet"
    }
    struct HttpMethods {
      static let get = "GET"
    }
  }
  struct JSONKeys {
    struct VideoInfo {
      /// `title`.
      static let title = "title"
      /// Image `rowArray`.
      static let rowArray = "rows"
      struct Rows {
        static let title = "title"
        static let description = "description"
        static let imageHref = "imageHref"
      }
    }
  }
  struct CellNames {
    static let imageInfoCell = "imageInfoCell"
  }
  struct Fonts {
    static let AvenirBook = "Avenir-Book"
  }
  struct FontSizes {
    static let small: CGFloat = 12
  }
}
